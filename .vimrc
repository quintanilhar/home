"Syntax ghlighting
syntax enable

"Enable filetype plugin
filetype plugin indent on

"Indentation behavior by file type
au FileType python setl sw=4 sts=4 et
"au FileType html setl sw=4 sts=4 et
"au FileType javascript setl sw=4 sts=4 et
"au FileType css setl sw=4 sts=4 et
"au FileType php setl sw=4 sts=4 et

"Highlight
set hlsearch

"Ruler (bottom/right)
set ruler

"Fixed lines when moving vertical
set so=7

"Enable mouse
set mouse=a
set ttymouse=xterm2

"Disable GUI toolbar
set guioptions-=T

"Don't save backup files
set nobackup
set nowritebackup
set noswapfile

"Show line numbers on the left
set number

"Shiftsel
set selectmode=mouse,key
set mousemodel=popup
set keymodel=startsel,stopsel
set selection=exclusive
set fileencoding=latin1

"Key modifications
map <F2> :NERDTreeToggle<CR>
map <F12> :NERDTreeFromBookmark 
map <C-F> :LocateTab 
map <C-T> :tabnew<CR> 

let locateopen_database = "~/svn/ldb"
let locateopen_exactly = 0
