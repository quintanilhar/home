PROMPT=$'%n@%m:%~$ '
if [[ -z "$TMUX" ]]; then
tmux new-session -d -s rquintanilha
tmux new-window -t rquintanilha:1 'vim'
tmux select-window -t rquintanilha:0
tmux attach-session -t rquintanilha
fi
